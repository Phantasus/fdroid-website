---
layout: post
title: "Fairphone Open ships F-Droid Privileged Extension!"
author: "eighthave"

---

Fairphone is taking a big steps towards making a fully free Android
phone!  They already have Fairphone Open, an alternate version of
Android that does not include any proprietary apps.  Their Fairphone
Updater makes it really easy to switch a Fairphone away from the
Google Android version to Fairphone Open.  And now, Fairphone Open
includes F-Droid!  That means it is a fully functional device after
the switch, there is no longer any need to flash extra packages or
manually install apps via "Unknown Sources".

The first version includes just F-Droid
[Privileged Extension](https://gitlab.com/fdroid/privileged-extension)
built into Fairphone Open. So you will still have to download F-Droid
and install it as a regular app.  It will automatically find and use
the Privileged Extension built into Fairphone Open.  Then your
Fairphone Open can automatically update apps and it'll have a smoother
install experience where you just need to press install once, and the
whole process will finish in the background.

These are now the step you need to do to have the full F-Droid
experience with Fairphone Open:

1. download [FDroid.apk](https://f-droid.org/FDroid.apk)
2. enable "Unknown Sources" in Settings -> Security
3. find _FDroid.apk_ in Downloads and click to install it
4. confirm the install on the system's install screen
5. disable "Unknown Sources"
