#!/usr/bin/env python3

import collections
import glob
import json
import os
import re


basedir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

with open(os.path.join(basedir, '_data', 'strings.json')) as fp:
    source = json.load(fp)

for f in glob.glob(os.path.join(basedir, '_data', '*', 'strings.json')):
    print(f)
    with open(f) as fp:
        translation = json.load(fp)
    new = collections.OrderedDict()
    for k in translation.keys():
        if k in source:
            new[k] = translation[k]
    with open(f, 'w', encoding='utf-8') as fp:
        json.dump(new, fp, indent=4, sort_keys=True, ensure_ascii=False)

    # re-add the trailing space, to minimize diffs with Weblate
    with open(f, 'rb') as fp:
        body = fp.read()
    body = re.sub(b'",', b'", ', body)
    body = re.sub(b'},', b'}, ', body)
    with open(f, 'wb') as fp:
        fp.write(body)
